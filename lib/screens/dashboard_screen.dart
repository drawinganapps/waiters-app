import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:restaurant/controller/menu_controller.dart';
import 'package:restaurant/data/dummy_data.dart';
import 'package:restaurant/helper/color_helper.dart';
import 'package:restaurant/helper/grand_total_helper.dart';
import 'package:restaurant/models/menu_filter.dart';
import 'package:restaurant/models/menu_model.dart';
import 'package:restaurant/routes/AppRoutes.dart';
import 'package:restaurant/widgets/default_loading_widget.dart';
import 'package:restaurant/widgets/filter_widget.dart';
import 'package:restaurant/widgets/menu_card_widget.dart';
import 'package:restaurant/widgets/search_widget.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = Get.width;
    final List<MenuFilter> menuFilter = DummyData.filterList;


    return GetBuilder<MenuController>(builder: (controller) {
      final List<MenuModel> menus = DummyData.menus.where((element) => element.type == menuFilter[controller.selectedFilter].type).toList();
      return Scaffold(
          appBar: AppBar(
            backgroundColor: ColorHelper.whiteDarker,
            elevation: 0,
            leadingWidth: 65,
            leading: Container(
              padding: const EdgeInsets.only(left: 15, top: 5, bottom: 5),
              child: IconButton(
                onPressed: () {},
                icon: const Icon(Icons.menu, color: Colors.black, size: 35),
              ),
            ),
            actions: [
              Container(
                width: 90,
                padding: const EdgeInsets.only(right: 15, bottom: 5, top: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                          color: Colors.black, shape: BoxShape.circle),
                      padding: const EdgeInsets.all(8),
                      child:
                          Text('${controller.orderedMenu.length}', style: TextStyle(color: ColorHelper.white)),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.shopping_bag_outlined,
                          color: Colors.black, size: 25),
                    )
                  ],
                ),
              )
            ],
          ),
          body: Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                    left: screenWidth * 0.06, right: screenWidth * 0.06),
                margin: const EdgeInsets.only(bottom: 10, top: 10),
                child: const SearchWidget(),
              ),
              SizedBox(
                height: 80,
                child: ListView(
                  physics: const BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  children: List.generate(menuFilter.length, (index) {
                    return GestureDetector(
                      onTap: () {
                        context.loaderOverlay.show(widget: const DefaultLoadingWidget());
                        Future.delayed(const Duration(milliseconds: 500), () {
                          controller.changeFilter(index);
                          context.loaderOverlay.hide();
                        });
                      },
                      child: FilterCategoriesWidget(
                          isSelected: index == controller.selectedFilter,
                          filter: menuFilter[index]),
                    );
                  }),
                ),
              ),
              Expanded(
                child: GridView.count(
                    padding: EdgeInsets.only(
                        left: screenWidth * 0.06, right: screenWidth * 0.06),
                    crossAxisCount: 2,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                    childAspectRatio: 0.7,
                    physics: const BouncingScrollPhysics(),
                    children: [
                      ...List.generate(menus.length, (index) {
                        return MenuCardWidget(menu: menus[index],);
                      }),
                      Container(),
                      Container(),
                    ]
                ),
              ),
            ],
          ),
          floatingActionButton: Visibility(
            visible: controller.orderedMenu.isNotEmpty,
            child: Container(
              height: 60,
              width: screenWidth * 0.8,
              padding: const EdgeInsets.only(left: 30, right: 30),
              decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(30)
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: ((){
                      context.loaderOverlay
                          .show(widget: const DefaultLoadingWidget());
                      Future.delayed(const Duration(seconds: 1), () {
                        context.loaderOverlay.hide();
                        Get.toNamed(AppRoutes.ORDER);
                      });
                    }),
                    child: Text('Add to Order', style: TextStyle(
                        color: ColorHelper.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w900
                    )),
                  ),
                  Text('\$${GrandTotalHelper.countGrandTotal(controller.orderedMenu).toStringAsFixed(2)}', style: TextStyle(
                      color: ColorHelper.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w700
                  )),
                ],
              ),
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          bottomNavigationBar: Container(
            padding: EdgeInsets.only(
                left: screenWidth * 0.06, right: screenWidth * 0.06),
            decoration: BoxDecoration(
                color: ColorHelper.white,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
            height: 100,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      decoration: const BoxDecoration(
                          color: Colors.black, shape: BoxShape.circle),
                      padding: const EdgeInsets.all(0),
                      child: const Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 30,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(3),
                      child: const Text('Checkout'),
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.grid_view_rounded,
                      color: Colors.grey,
                      size: 30,
                    ),
                    Container(
                        padding: const EdgeInsets.all(3),
                        child: const Text('Tables'))
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.schedule_outlined,
                      color: Colors.grey,
                      size: 30,
                    ),
                    Container(
                        padding: const EdgeInsets.all(3),
                        child: const Text('Waiting list'))
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(
                      Icons.settings_outlined,
                      color: Colors.grey,
                      size: 30,
                    ),
                    Container(
                        padding: const EdgeInsets.all(3),
                        child: const Text('Settings'))
                  ],
                ),
              ],
            ),
          ));
    });
  }
}
