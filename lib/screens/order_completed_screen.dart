import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:restaurant/controller/menu_controller.dart';
import 'package:restaurant/controller/tables_controller.dart';
import 'package:restaurant/helper/color_helper.dart';
import 'package:restaurant/routes/AppRoutes.dart';
import 'package:restaurant/widgets/default_loading_widget.dart';

class OrderCompletedScreen extends StatelessWidget {
  const OrderCompletedScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<TablesController>();
    final menuController = Get.find<MenuController>();
    final screenWidth = Get.width;
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/icons/checked.png', width: 100, height: 100)
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 10, bottom: 20),
            child: const Text('Yor order has been placed!',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.w900)),
          ),
          Container(
            padding: EdgeInsets.only(
                left: screenWidth * 0.06, right: screenWidth * 0.06),
            child: Text(
                'Please wait and go to table number ${controller.selectedTable} while we serve your order Thanks!',
                textAlign: TextAlign.center,
                style: const TextStyle(
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.w500,
                    fontSize: 18)),
          )
        ],
      ),
      floatingActionButton: Container(
        height: 60,
        width: screenWidth * 0.8,
        padding: const EdgeInsets.only(left: 30, right: 30),
        decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.circular(30)
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: ((){
                context.loaderOverlay
                    .show(widget: const DefaultLoadingWidget());
                Future.delayed(const Duration(seconds: 1), () {
                  menuController.resetOrder();
                  controller.resetTable();
                  context.loaderOverlay.hide();
                  Get.toNamed(AppRoutes.HOME);
                });
              }),
              child: Text('Got It', style: TextStyle(
                  color: ColorHelper.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w900
              )),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
