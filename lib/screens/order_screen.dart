import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:restaurant/controller/menu_controller.dart';
import 'package:restaurant/controller/tables_controller.dart';
import 'package:restaurant/helper/color_helper.dart';
import 'package:restaurant/helper/grand_total_helper.dart';
import 'package:restaurant/routes/AppRoutes.dart';
import 'package:restaurant/widgets/default_loading_widget.dart';
import 'package:restaurant/widgets/ordered_menu_card.dart';

class OrderScreen extends StatelessWidget {
  const OrderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = Get.width;
    final screenHeight = Get.height;
    final tableController = Get.find<TablesController>();
    return GetBuilder<MenuController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: ColorHelper.whiteDarker,
          elevation: 0,
          leadingWidth: 65,
          title: const Text('Order',
              style: TextStyle(color: Colors.black, fontWeight: FontWeight.w900)),
          centerTitle: true,
          leading: Container(
            padding: const EdgeInsets.only(left: 15, top: 5, bottom: 5),
            child: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(Icons.arrow_back_ios_new_outlined,
                  color: Colors.black, size: 25),
            ),
          ),
          actions: [
            Container(
              width: 90,
              padding: const EdgeInsets.only(right: 15, bottom: 5, top: 5),
              child: Center(
                child: Text('#503',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        color: ColorHelper.grey)),
              ),
            )
          ],
        ),
        body: Column(
          children: [
            Container(
                padding: EdgeInsets.only(
                    left: screenWidth * 0.06, right: screenWidth * 0.06),
                margin: const EdgeInsets.only(top: 25, bottom: 25),
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Get.toNamed(AppRoutes.TABLES);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.black,
                            borderRadius: BorderRadius.circular(20)),
                        padding: const EdgeInsets.only(
                            top: 10, bottom: 10, left: 15, right: 15),
                        margin: const EdgeInsets.only(right: 15),
                        child: Row(
                          children: [
                            Text('Table',
                                style: TextStyle(
                                    color: ColorHelper.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 15)),
                            Container(
                              margin: const EdgeInsets.only(left: 25),
                              child: Icon(Icons.navigate_next_outlined,
                                  color: ColorHelper.white),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(20)),
                      padding: const EdgeInsets.only(
                          top: 10, bottom: 10, left: 15, right: 15),
                      child: Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(right: 15),
                            child: Icon(Icons.person, color: ColorHelper.white),
                          ),
                          Text('1',
                              style: TextStyle(
                                  color: ColorHelper.white,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 15)),
                        ],
                      ),
                    ),
                  ],
                )),
            Expanded(
                child: ListView(
                  padding: EdgeInsets.only(left: screenWidth * 0.06, right: screenWidth * 0.06),
                  physics: const BouncingScrollPhysics(),
                  children: List.generate(controller.orderedMenu.length, (index) {
                    return OrderedMenuCard(orderedMenuModel: controller.orderedMenu[index]);
                  }),
                )),
            Container(
              padding: EdgeInsets.only(
                  left: screenWidth * 0.06, right: screenWidth * 0.06),
              margin: EdgeInsets.only(bottom: screenHeight * 0.12),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text('Discount', style: TextStyle(fontSize: 18)),
                      Container(
                        decoration: const BoxDecoration(
                            color: Colors.black, shape: BoxShape.circle),
                        child: Icon(Icons.add, color: ColorHelper.white),
                      )
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    decoration: const BoxDecoration(
                        border: Border(
                            top: BorderSide(width: 1, color: Colors.black))),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text('Total',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      Text(
                        '\$${GrandTotalHelper.countGrandTotal(controller.orderedMenu).toStringAsFixed(2)}',
                        style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w700),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        floatingActionButton: Visibility(
          visible: controller.orderedMenu.isNotEmpty,
          child: Container(
            height: 60,
            width: screenWidth * 0.8,
            padding: const EdgeInsets.only(left: 30, right: 30),
            decoration: BoxDecoration(
                color: Colors.black, borderRadius: BorderRadius.circular(30)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: (() {
                    if (tableController.selectedTable == 0) {
                      Get.toNamed(AppRoutes.TABLES);
                      return;
                    }
                    context.loaderOverlay
                        .show(widget: const DefaultLoadingWidget());
                    Future.delayed(const Duration(seconds: 1,), () {
                      context.loaderOverlay.hide();
                      Get.toNamed(AppRoutes.COMPLETED);
                    });
                  }),
                  child: Text('Order Now',
                      style: TextStyle(
                          color: ColorHelper.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w900)),
                ),
                Text(
                    '\$${GrandTotalHelper.countGrandTotal(controller.orderedMenu).toStringAsFixed(2)}',
                    style: TextStyle(
                        color: ColorHelper.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w700)),
              ],
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
    });
  }
}
