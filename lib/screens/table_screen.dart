import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:loader_overlay/loader_overlay.dart';
import 'package:restaurant/controller/tables_controller.dart';
import 'package:restaurant/helper/color_helper.dart';
import 'package:restaurant/widgets/default_loading_widget.dart';
import 'package:restaurant/widgets/left_table_widget.dart';
import 'package:restaurant/widgets/main_table_widget.dart';
import 'package:restaurant/widgets/rigth_table_widget.dart';

class TableScreen extends StatelessWidget {
  const TableScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TablesController>(builder: (controller){
      final screenWidth = Get.width;
      return Scaffold(
        appBar: AppBar(
            backgroundColor: ColorHelper.whiteDarker,
            elevation: 0,
            leadingWidth: 65,
            title: const Text('Tables',
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.w900)),
            centerTitle: true,
            leading: Container(
              padding: const EdgeInsets.only(left: 15, top: 5, bottom: 5),
              child: IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: const Icon(Icons.arrow_back_ios_new_outlined,
                    color: Colors.black, size: 25),
              ),
            )),
        body: Column(
          children: [
            Container(
              height: 45,
              margin: EdgeInsets.only(
                  left: screenWidth * 0.06, right: screenWidth * 0.06, top: 20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.grey.withOpacity(0.1)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 180,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.black),
                    child: TextButton(
                        onPressed: () {},
                        child: Text('Main Room',
                            style: TextStyle(
                                color: ColorHelper.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16))),
                  ),
                  SizedBox(
                    width: 165,
                    child: TextButton(
                        onPressed: () {},
                        child: const Text('Terrace',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 16))),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                LeftTableWidget(tableNumber: 3),
                MainTableWidget(tableNumber: 6),
                RightTableWidget(tableNumber: 9)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                LeftTableWidget(tableNumber: 4),
                MainTableWidget(tableNumber: 7),
                RightTableWidget(tableNumber: 10)
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                LeftTableWidget(tableNumber: 5),
                MainTableWidget(tableNumber: 8),
                RightTableWidget(tableNumber: 11)
              ],
            ),
          ],
        ),
        floatingActionButton: Visibility(
          visible: controller.selectedTable != 0,
          child: Container(
            height: 60,
            width: screenWidth * 0.8,
            padding: const EdgeInsets.only(left: 30, right: 30),
            decoration: BoxDecoration(
                border: Border.all(width: 2, color: Colors.black),
                borderRadius: BorderRadius.circular(30)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: (() {
                    final snackBar = SnackBar(
                      backgroundColor: Colors.green.shade200,
                      behavior: SnackBarBehavior.floating,
                      content: Text(
                          'Yey!! Successfully Booked Table number ${controller.selectedTable}',
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.w500)),
                    );
                    context.loaderOverlay
                        .show(widget: const DefaultLoadingWidget());
                    Future.delayed(const Duration(seconds: 1), () {
                      context.loaderOverlay.hide();
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      Get.back();
                    });
                  }),
                  child: const Text('Select a table',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 22,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
    });
  }
}
