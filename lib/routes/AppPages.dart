import 'package:get/get.dart';
import 'package:restaurant/controller/menu_binding.dart';
import 'package:restaurant/controller/tables_binding.dart';
import 'package:restaurant/screens/dashboard_screen.dart';
import 'package:restaurant/screens/order_completed_screen.dart';
import 'package:restaurant/screens/order_screen.dart';
import 'package:restaurant/screens/table_screen.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.HOME,
        page: () => const DashboardScreen(),
        binding: MenuBinding(),
        transition: Transition.rightToLeft),
    GetPage(
        name: AppRoutes.ORDER,
        page: () => const OrderScreen(),
        bindings: [MenuBinding(), TablesBinding()],
        transition: Transition.rightToLeft),
    GetPage(
        name: AppRoutes.TABLES,
        page: () => const TableScreen(),
        binding: TablesBinding(),
        transition: Transition.rightToLeft),
    GetPage(
        name: AppRoutes.COMPLETED,
        page: () => const OrderCompletedScreen(),
        transition: Transition.rightToLeft),
  ];
}
