class AppRoutes {
  static const String HOME = '/';
  static const String ORDER = '/order';
  static const String TABLES = '/tables';
  static const String COMPLETED = '/completed';
}
