import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:restaurant/helper/color_helper.dart';

ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
  scaffoldBackgroundColor: ColorHelper.whiteDarker,
  highlightColor: Colors.transparent,
  splashColor: Colors.transparent,
  textTheme: GoogleFonts.latoTextTheme().copyWith(
    bodyText1: GoogleFonts.notoSerif(),
  ),
);
