import 'package:restaurant/models/menu_filter.dart';
import 'package:restaurant/models/menu_model.dart';

class DummyData {
  static List<MenuFilter> filterList = <MenuFilter>[
    const MenuFilter('Rice', 'foods', 'rice.png', MenuType.foods),
    const MenuFilter('Drinks', 'drinks', 'cheers.png', MenuType.drinks),
    const MenuFilter('Pizza', 'foods', 'pizza.png', MenuType.pizza),
    const MenuFilter('Additional', 'foods', 'egg.png', MenuType.foods),
    const MenuFilter('Dessert', 'foods', 'cupcake.png', MenuType.foods),
  ];

  static List<MenuModel> menus = <MenuModel>[
    MenuModel(id: 1, menuName: 'Nasi Goreng', menuPrice: 4.5, menuImage: 'nasigoreng.png', type: MenuType.foods),
    MenuModel(id: 2, menuName: 'Grace Salad', menuPrice: 3.5, menuImage: 'salad-yunani.png', type: MenuType.foods),
    MenuModel(id: 3, menuName: 'Ramen', menuPrice: 5.6, menuImage: 'ramen.png', type: MenuType.foods),
    MenuModel(id: 4, menuName: 'Sate', menuPrice: 7.2, menuImage: 'sate.png', type: MenuType.foods),
    MenuModel(id: 5, menuName: 'Cola', menuPrice: 1.2, menuImage: 'cola-cola.png', type: MenuType.drinks),
    MenuModel(id: 6, menuName: 'Mineral Water', menuPrice: 0.5, menuImage: 'mineral-water.png', type: MenuType.drinks),
    MenuModel(id: 7, menuName: 'Orange Juice', menuPrice: 2.2, menuImage: 'orange-jus.png', type: MenuType.drinks),
    MenuModel(id: 8, menuName: 'Apple Juice', menuPrice: 2.5, menuImage: 'apple-jus.png', type: MenuType.drinks),
    MenuModel(id: 9, menuName: 'Large Meat Ball', menuPrice: 2.5, menuImage: 'meatball.png', type: MenuType.foods),
    MenuModel(id: 10, menuName: 'Mie Goreng', menuPrice: 2.5, menuImage: 'mie-goreng.png', type: MenuType.foods),
    MenuModel(id: 11, menuName: 'Italian Pizza', menuPrice: 2.5, menuImage: 'pizza1.png', type: MenuType.pizza),
    MenuModel(id: 12, menuName: 'Pizza With Double Sauce', menuPrice: 2.5, menuImage: 'pizza2.png', type: MenuType.pizza),
    MenuModel(id: 13, menuName: 'Beef Burger', menuPrice: 2.5, menuImage: 'beef-burger.png', type: MenuType.pizza),
    MenuModel(id: 14, menuName: 'Double Beef Burger', menuPrice: 2.5, menuImage: 'double-beef-burger.png', type: MenuType.pizza),
    MenuModel(id: 15, menuName: 'Mini Kebab', menuPrice: 2.5, menuImage: 'kebab1.png', type: MenuType.pizza),
    MenuModel(id: 16, menuName: 'Kebab Double', menuPrice: 2.5, menuImage: 'kebab2.png', type: MenuType.pizza),
  ];
}