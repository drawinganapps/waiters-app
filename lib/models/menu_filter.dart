import 'package:restaurant/models/menu_model.dart';

class MenuFilter {
  const MenuFilter(this.name, this.initials, this.icon, this.type);

  final String name;
  final String initials;
  final String icon;
  final MenuType type;
}
