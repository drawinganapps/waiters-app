import 'package:flutter/foundation.dart';

class MenuModel {
  MenuModel({
    required this.id,
    required this.menuName,
    required this.menuPrice,
    required this.menuImage,
    required this.type
  });

  final int id;
  final String menuName;
  final String menuImage;
  final double menuPrice;
  final MenuType type;
}

class OrderedMenuModel {
  late MenuModel menu;
  late int quantity;

  OrderedMenuModel(this.menu, this.quantity);
}

enum MenuType {
  foods,
  drinks,
  pizza
}