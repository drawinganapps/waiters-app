import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:restaurant/controller/tables_controller.dart';
import 'package:restaurant/helper/color_helper.dart';

class LeftTableWidget extends StatelessWidget {
  final int tableNumber;
  const LeftTableWidget({Key? key, required this.tableNumber}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TablesController>(builder: (controller){
      final bool selected = controller.selectedTable == tableNumber;
      return GestureDetector(
        onTap: () {
          controller.changeTable(tableNumber);
        },
        child: Container(
          padding: const EdgeInsets.all(15),
          height: 190,
          child: Stack(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 65,
                    height: 35,
                    margin: const EdgeInsets.only(bottom: 5),
                    decoration: BoxDecoration(
                        color: selected ? Colors.black : ColorHelper.grey,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  Container(
                    width: 65,
                    height: 35,
                    margin: const EdgeInsets.only(bottom: 5),
                    decoration: BoxDecoration(
                        color: selected ? Colors.black : ColorHelper.grey,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ],
              ),
              Positioned(
                  top: 27,
                  child: Container(
                    height: 100,
                    width: 50,
                    margin: const EdgeInsets.only(right: 8),
                    padding: const EdgeInsets.only(left: 5, right: 10),
                    decoration: BoxDecoration(
                        color: selected ? Colors.black : ColorHelper.grey,
                        borderRadius: BorderRadius.circular(20),
                        border:
                        Border.all(width: 2, color: ColorHelper.whiteDarker)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text('$tableNumber', style: TextStyle(
                            color: ColorHelper.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold
                        ))
                      ],
                    ),
                  )),
            ],
          ),
        ),
      );
    });
  }
}
