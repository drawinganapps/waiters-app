import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:restaurant/controller/tables_controller.dart';
import 'package:restaurant/helper/color_helper.dart';

class MainTableWidget extends StatelessWidget {
  final int tableNumber;

  const MainTableWidget({Key? key, required this.tableNumber})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<TablesController>(builder: (controller) {
      final bool isSelected = controller.selectedTable == tableNumber;
      return GestureDetector(
        onTap: () {
          controller.changeTable(tableNumber);
        },
        child: Container(
          padding: const EdgeInsets.all(15),
          width: 190,
          child: Stack(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 35,
                    height: 80,
                    margin: const EdgeInsets.only(right: 5),
                    decoration: BoxDecoration(
                        color: isSelected ? Colors.black : ColorHelper.grey,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  Container(
                    width: 35,
                    height: 80,
                    margin: const EdgeInsets.only(right: 5),
                    decoration: BoxDecoration(
                        color: isSelected ? Colors.black : ColorHelper.grey,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                  Container(
                    width: 35,
                    height: 80,
                    margin: const EdgeInsets.only(right: 5),
                    decoration: BoxDecoration(
                        color: isSelected ? Colors.black : ColorHelper.grey,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ],
              ),
              Positioned(
                  top: 15,
                  child: Container(
                    width: 150,
                    height: 50,
                    margin: const EdgeInsets.only(right: 8),
                    decoration: BoxDecoration(
                        color: isSelected ? Colors.black : ColorHelper.grey,
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(
                            width: 2, color: ColorHelper.whiteDarker)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('$tableNumber',
                            style: TextStyle(
                                color: ColorHelper.white,
                                fontSize: 18,
                                fontWeight: FontWeight.bold))
                      ],
                    ),
                  )),
            ],
          ),
        ),
      );
    });
  }
}
