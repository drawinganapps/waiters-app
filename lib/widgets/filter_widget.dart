import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:restaurant/helper/color_helper.dart';
import 'package:restaurant/models/menu_filter.dart';

class FilterCategoriesWidget extends StatelessWidget {
  final bool isSelected;
  final MenuFilter filter;

  const FilterCategoriesWidget(
      {Key? key, required this.isSelected, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = Get.width;

    return Center(
      child: Container(
        margin: EdgeInsets.only(left: screenWidth * 0.06, right: screenWidth * 0.06),
        child: Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: ColorHelper.white,
            shape: BoxShape.circle,
            border: Border.all(color: isSelected ? Colors.black : Colors.transparent , width: 2)
          ),
          child: Image.asset('assets/icons/${filter.icon}', height: 35, width: 35),
        ),
      ),
    );
  }
}
