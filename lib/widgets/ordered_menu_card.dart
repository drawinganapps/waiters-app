import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:restaurant/controller/menu_controller.dart';
import 'package:restaurant/helper/color_helper.dart';
import 'package:restaurant/models/menu_model.dart';

class OrderedMenuCard extends StatelessWidget {
  final OrderedMenuModel orderedMenuModel;

  const OrderedMenuCard({Key? key, required this.orderedMenuModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MenuController>(builder: (controller){
      return Container(
        padding: const EdgeInsets.only(left: 10, right: 20),
        margin: const EdgeInsets.only(bottom: 15),
        decoration: BoxDecoration(
            color: ColorHelper.white, borderRadius: BorderRadius.circular(30)),
        child: Row(
          children: [
            Container(
              margin: const EdgeInsets.only(right: 15),
              child: Image.asset(
                'assets/images/${orderedMenuModel.menu.menuImage}',
                width: 120,
                height: 140,
              ),
            ),
            Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: const EdgeInsets.only(bottom: 8),
                          child: Text(orderedMenuModel.menu.menuName,
                              style: const TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.w500)),
                        ),
                        Text('\$${orderedMenuModel.menu.menuPrice}',
                            style: const TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w800)),
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(bottom: 10),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Add note',
                            style: TextStyle(
                                color: ColorHelper.grey,
                                fontStyle: FontStyle.italic)),
                        Container(
                          width: 90,
                          margin: const EdgeInsets.only(top: 10),
                          decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(20)),
                          padding: const EdgeInsets.only(
                              left: 8, right: 8, top: 5, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: ColorHelper.white, shape: BoxShape.circle),
                                child: SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: IconButton(
                                      onPressed: () {
                                        controller.removeOrderedMenu(orderedMenuModel.menu);
                                      },
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(0),
                                      icon: const Icon(Icons.remove_outlined,
                                          color: Colors.grey)),
                                ),
                              ),
                              Text('${orderedMenuModel.quantity}',
                                  style: TextStyle(
                                      color: ColorHelper.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w600)),
                              Container(
                                decoration: BoxDecoration(
                                    color: ColorHelper.white, shape: BoxShape.circle),
                                child: SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: IconButton(
                                      onPressed: () {
                                        controller.addOrderedMenu(orderedMenuModel.menu);
                                      },
                                      padding: const EdgeInsets.all(0),
                                      iconSize: 20,
                                      icon:
                                      const Icon(Icons.add, color: Colors.grey)),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ))
          ],
        ),
      );
    });
  }
}
