import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:restaurant/controller/menu_controller.dart';
import 'package:restaurant/helper/color_helper.dart';
import 'package:restaurant/models/menu_model.dart';

class MenuCardWidget extends StatelessWidget {
  final MenuModel menu;

  const MenuCardWidget({Key? key, required this.menu}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = Get.width;
    final height = Get.width;
    return GetBuilder<MenuController>(builder: (controller) {
      var itemCount = controller.orderedMenu.firstWhereOrNull((element) => element.menu.id == menu.id);
      return Stack(
        children: [
          Container(
            width: width,
            decoration: BoxDecoration(
                color: ColorHelper.white,
                borderRadius: BorderRadius.circular(30)),
            margin: EdgeInsets.only(top: height * 0.09),
            padding: EdgeInsets.all(width * 0.05),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(menu.menuName),
                Container(
                  margin: const EdgeInsets.only(top: 5),
                ),
                Text('\$${menu.menuPrice}',
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 15)),
                Container(
                  width: 90,
                  margin: const EdgeInsets.only(top: 10),
                  decoration: BoxDecoration(
                      color: itemCount != null ? Colors.black : ColorHelper.grey,
                      borderRadius: BorderRadius.circular(20)),
                  padding: const EdgeInsets.all(8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: ColorHelper.white, shape: BoxShape.circle),
                        child: SizedBox(
                          height: 25,
                          width: 25,
                          child: IconButton(
                              onPressed: () {
                                controller.removeOrderedMenu(menu);
                              },
                              padding: const EdgeInsets.all(0),
                              icon: const Icon(Icons.remove_outlined, color: Colors.grey)),
                        ),
                      ),
                      Text('${itemCount != null ? itemCount.quantity : 0}',
                          style: TextStyle(
                              color: ColorHelper.white,
                              fontSize: 15,
                              fontWeight: FontWeight.bold)),
                      Container(
                        decoration: BoxDecoration(
                            color: ColorHelper.white, shape: BoxShape.circle),
                        child: SizedBox(
                          height: 25,
                          width: 25,
                          child: IconButton(
                              onPressed: () {
                                controller.addOrderedMenu(menu);
                              },
                              padding: const EdgeInsets.all(0),
                              icon: const Icon(Icons.add, color: Colors.grey)),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Positioned(
              top: 0,
              left: width * 0.02,
              child: Image.asset('assets/images/${menu.menuImage}',
                  width: 150, height: 100, fit: BoxFit.fill)),
        ],
      );
    });
  }
}
