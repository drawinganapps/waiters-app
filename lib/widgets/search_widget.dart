import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant/helper/color_helper.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 60,
      child: TextField(
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 20),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: ColorHelper.white, width: 0.3)),
            filled: true,
            fillColor: ColorHelper.white,
            hintText: 'Search',
            prefixIcon: Icon(Icons.search, color: ColorHelper.grey, size: 30),
            hintStyle: TextStyle(
                fontWeight: FontWeight.w400, color: ColorHelper.grey)),
      ),
    );
  }
}
