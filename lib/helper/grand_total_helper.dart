import 'package:restaurant/models/menu_model.dart';

class GrandTotalHelper {
  static double countGrandTotal(List<OrderedMenuModel> orderedMenu) {
    var countTotal = 0.0;
    for (var element in orderedMenu) {
      final total = element.quantity * element.menu.menuPrice;
      countTotal = countTotal + total;
    }
    return countTotal;
  }
}