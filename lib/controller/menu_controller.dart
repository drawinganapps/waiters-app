import 'package:get/get.dart';
import 'package:restaurant/models/menu_model.dart';

class MenuController extends GetxController {
  var selectedFilter = 0;
  List<OrderedMenuModel> orderedMenu = [];

  void changeFilter(int index)  {
    selectedFilter = index;
    update();
  }

  void addOrderedMenu(MenuModel menu) {
    var currentOrder = orderedMenu.indexWhere((element) {
      return element.menu.id == menu.id;
    });

    if (currentOrder > -1) {
      orderedMenu[currentOrder].quantity += 1;
      update();
      return;
    }
    orderedMenu.add(OrderedMenuModel(menu, 1));
    update();
  }

  void removeOrderedMenu(MenuModel menu) {
    var currentOrder = orderedMenu.indexWhere((element) {
      return element.menu.id == menu.id;
    });

    if (currentOrder < 0) {
      return;
    }

    if (orderedMenu[currentOrder].quantity < 2) {
      orderedMenu.removeAt(currentOrder);
      update();
      return;
    }

    orderedMenu[currentOrder].quantity -= 1;
    update();
  }

  void resetOrder() {
    orderedMenu = [];
    update();
  }
}
