import 'package:get/get.dart';

class TablesController extends GetxController {
  var selectedTable = 0;

  void changeTable(int tableNumber)  {
    selectedTable = tableNumber;
    update();
  }

  void resetTable() {
    selectedTable = 0;
    update();
  }
}
